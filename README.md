# Little Fighter 2 AI ScriptEngine (Neora Support)

This fork brings support for AI in Little Fighter 2 [Neora](https://lf-empire.de/forum/showthread.php?tid=9499), which is an edited EXE edition of [Little Fighter 2](http://lf2.net/).

This repository is a fork of the ai scriptengine by seikosantana, who forked the project by zort, who forked by Silva/Boop/Doix and SomeoneElse.

**Visualization of the fork tree:**
 * [Silva/Boop/Dox & SomeoneElse](https://lf-empire.de/forum/showthread.php?tid=7927)
   * [zort](https://github.com/zort/lf2-ai-scriptengine)
     * [seikosantana](https://github.com/seikosantana/lf2-ai-scriptengine)
       * [Luigi600](https://gitlab.com/Luigi600/little-fighter-2-neora-ai/)

<br />
<br />

## Download

The normal user can already download the compiled DLL from here. Under [Releases](https://gitlab.com/Luigi600/little-fighter-2-scriptengine-neora/-/releases) there is a list of versions. The top version is always the latest version. Make sure that you do not download the "Source Code", but the "dll-compiled-XYZ" in the "Other" category under the source code.

### Own compilation

If you are familiar with C++, you can also simply clone this project and compile it yourself. This project uses Visual Studio Build Tools 2017. The SDK version used can be customized.

<br />
<br />

## To Check

Possibly the following values are not reachable or simply wrong:

| Attribute | C++ Structure | Checked/Fixed |
| --------- | ------------- | ------------- |
| reserve | sObject | - |
| pic_gain | sObject | - |
| bottle_hp | sObject | - |
| armour_multiplier | sObject | - |
| * | sStage | - |
| * | sPhase | - |
| * | sSpawn | - |
| sound | sFrame | - (looks really strange) |

**Legend:**  
X = value found, checked and if necessary already correctly positioned  
\- = still missing

<br />
<br />

## Bugs / Known Issues / Possible Problems

It may very well be that the VRest in the AI is wrong. This is partly because I am not sure where it takes this value from (magic number of 0xF0 and/or 0xF3).
Furthermore, the compiled version may not fit every system. During the development it happened very rarely that Little Fighter crashed. When exactly this happens I could not find out yet.

<br />
<br />

## Authors

The main developer(s) are of course the ones from whom the project has been forked. Such a big thank you to them! The support for Neora has been made by Luigi600.

<br />
<br />

## Donation

If you like my work, you can donate for me. By doing so, you show that my work is valued.
A donation is not mandatory, but I would be very happy :)

| Service / Currency | Address | Link / QR Code |
| ------------------ | ------- | -------------- |
| Bitcoin | 39S7NWad3FyGVN43etz4K45s9MBaYj8sbp | ![Bitcoin Donation Address](.gitlab/doc/imgs/donation_bitcoin.png) |
| Ethereum | 0xBf6722333D72Bd1C0B3C6B3ad970310D1Ea6E83B | ![Ethereum Donation Address](.gitlab/doc/imgs/donation_ethereum.png) |
| Litecoin | MCi3b77HVTvJFAcDZ3wASsmJRJrmxGgzKj | ![Litecoin Donation Address](.gitlab/doc/imgs/donation_litecoin.png) |
| PayPal | payment [at] lui-studio.net | ![PayPal Donation Address](.gitlab/doc/imgs/donation_paypal.png) <br /> <form action="https://www.paypal.com/donate" method="post" target="_top"><input type="hidden" name="hosted_button_id" value="MBKLD79X3SCT2" /><input type="image" src="https://www.paypalobjects.com/en_US/DE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" /><img alt="" border="0" src="https://www.paypal.com/en_DE/i/scr/pixel.gif" width="1" height="1" /></form> |
